{
    "moduleFileExtensions": [
        "js",
        "jsx"
    ],
    "moduleNameMapper": {
        "CategoriesIndex": "<rootDir>/src/components/categories_index.js",
        "CategoryShow": "<rootDir>/src/components/category_show.js"
    }
}
