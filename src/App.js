import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import { BrowserRouter, Route, Switch, NoMatch } from 'react-router-dom'
import { media } from './components/media_template'
import promise from 'redux-promise'
import reducers from './reducers'
import styled from 'styled-components'
import CategoryShow from './components/category_show'
import CategoriesIndex from './components/categories_index'
import logo from './logo.png';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore)
const Container = styled.div`
  margin: 0 auto;
  color: #333;
  font-family: "Courier New", Arial, sans-serif;
  display: flex;
  flex-flow: column;
  ${media.desktop`padding: 0 1.25em;`}
  ${media.tablet`padding: 0 0.62em;`}
  ${media.phone`padding: 0 0.37em;`}
`
const StyledHeader = styled.div `
  width: 100%;
  display: flex;
  justify-content: center;
  margin-top: 10px;
`
const StyledTitle = styled.h1 `
  width: 300px;
  color: transparent;
  font-size: 0.5em;
`
const StyledImg = styled.img `
  max-width: 100%;
  width: 100%;
`
class App extends Component {
  render() {
    return (
      <Provider store={createStoreWithMiddleware(reducers)}>
        <Container>
          <StyledHeader>
            <StyledTitle className="app-logo">
              <StyledImg src={logo} alt="Chuck Norris J0kes"/>
              Chuck Norris Jokes
            </StyledTitle>
          </StyledHeader>
          <BrowserRouter forceRefresh={true}>
            <div>
              <Switch>
                <Route path="/:id" component={CategoryShow} />
                <Route exact path="/" component={CategoriesIndex} />
                <Route component={CategoriesIndex}/>
              </Switch>
            </div>
          </BrowserRouter>
        </Container>
      </Provider>
    );
  }
}

export default App;
