import { FETCH_CATEGORIES } from '../actions'
import { FETCH_CATEGORY } from '../actions'

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_CATEGORY:
      return { ...state, [action.payload.data.category]: action.payload.data }
    case FETCH_CATEGORIES:
      return action.payload.data
    default:
      return state
  }
}
