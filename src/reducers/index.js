import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import CategoriesReducer from './reducer_categories'

const rootReducer = combineReducers({
  categories: CategoriesReducer
})

export default rootReducer
