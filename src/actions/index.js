import axios from 'axios'

export const FETCH_CATEGORIES = 'fetch_categories'
export const FETCH_CATEGORY = 'fetch_category'

const ROOT_URL = 'https://api.chucknorris.io/jokes'

export function fetchCategories() {
  const request = axios.get(`${ROOT_URL}/categories`)
  return {
    type: FETCH_CATEGORIES,
    payload:request
  }
}

export function fetchCategory(category) {
  const request = axios.get(`${ROOT_URL}/random?category=${category}`)
  return {
    type: FETCH_CATEGORY,
    payload:request
  }
}
