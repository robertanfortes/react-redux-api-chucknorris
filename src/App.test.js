import React from 'react';
import ReactDOM from 'react-dom';
import CategoriesIndex from '../src/components/categories_index';
import CategoryShow from '../src/components/category_show';

describe('CategoriesIndex', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<CategoriesIndex/>, div);
    });
});


describe('CategoryShow', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<CategoryShow/>, div);
    });
});
