import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { fetchCategory } from '../actions'
import styled from 'styled-components'
import { media } from './media_template'

const Content = styled.div `
  display: flex;
  flex-flow: column;
  align-items: center;
  width: 100%;
`
const StyledDiv = styled.div `
  display: flex;
  align-items:flex-start;
  width: 50%;
  border: 1px solid #dddddd;
  background: #ffffff;
  box-shadow: 0 1px 1px 0 rgba(0,0,0,0.05);
  padding: 2em;
  ${media.tablet`width:80%;`}
  & .content-joke {
    margin: 0 0 0 1.25em;
  }
`
const StyledLink = styled(Link) `
  margin-top: 3em;
  color: rgb(241, 90, 36);
  &:hover {
    opacity: 0.7;
  }
`
class CategoryShow extends Component {

  componentDidMount() {
    const { id } = this.props.match.params
    this.props.fetchCategory(id)
  }

  render() {
    const { category } = this.props
    if (!category) {
      return <Content>
                <span>
                  Loading...
                </span>
                <StyledLink to="/">Back to categories</StyledLink>
             </Content>
    }
    return (
      <Content>
        <h3>Category: {category.category}</h3>
        <StyledDiv>
          <img src={category.icon_url}/>
          <h3 className="content-joke">{category.value}</h3>
        </StyledDiv>
        <StyledLink to="/">Back to categories</StyledLink>
      </Content>
    )
  }
}

function mapStateToProps({ categories }, ownProps) {
  return { category: categories[ownProps.match.params.id] }
}

export default connect(mapStateToProps, { fetchCategory }) (CategoryShow)
