import _ from 'lodash'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { fetchCategories } from '../actions'
import styled from 'styled-components'
import { media } from './media_template'

const StyledUl = styled.ul `
  margin: 0;
  padding: 0;
  width: 100%;
  display: flex;
  flex-flow: column;
  align-items: center;
`
const StyledLi = styled.li`
  border-radius: 2px;
  color: white;
  border: 1px solid #dddddd;
  padding: 1em;
  margin-bottom: 0.2em;
  box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.05);
  width: 80%;
  list-style: none;
  ${media.desktop`padding: 1.5em;`}
`
const StyledLink = styled(Link) `
  text-decoration: none;
  color: rgb(241, 90, 36);
  width: 100%;
  display: block;
  font-weight: bold;
  &:hover {
    opacity: 0.7;
  }
`

class CategoriesIndex extends Component {

  componentDidMount() {
    this.props.fetchCategories()
  }

  renderCategories() {
    return _.map(this.props.categories, (category,id) => {
      return (
          <StyledLi key={id}>
            <StyledLink to={`/${category}`}>
              {category}
            </StyledLink>
          </StyledLi>
      )
    })
  }

  render() {
    return (
      <div>
        <StyledUl>
          {this.renderCategories()}
        </StyledUl>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return { categories: state.categories }
}

export default connect(mapStateToProps, { fetchCategories })(CategoriesIndex)
